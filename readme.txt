
-- INSTALLATION --

Go to Site Building > Modules > enable Media: Vzaar.

-- CONFIGURATION --

You can find all Vzaar settings in
Content Management > Embedded Media Field configuration > Embedded Video Field > Vzaar configuration

-- FEATURES --

* Works with multiple url patterns ie: http://vzaar.com/videos/xxxxxx, http://vzaar.com/users/UserID/videos/xxxxxx
* Also works with embed code
* Support playlist ie: http://view.vzaar.com/users/UserID.flashplaylist
* Brand Text and End 
* Different colour settings and player options

-- CONTACT --

Current maintainers:
* Alli Price (alli.price) - http://drupal.org/user/431193

This project has been sponsored by:
* Deeson eMedia
  Visit http://www.deeson.co.uk/emedia for more information.