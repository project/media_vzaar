<?php
/**
 * @file vzaar.inc
 * This is an include file used by emfield.module for vzaar content.
 */

// This is the main URL for your provider.
define('EMVIDEO_VZAAR_MAIN_URL', 'http://vzaar.com/');

// Vzaar version. This is just for emvideo.
define('EMVIDEO_VZAAR_DATA_VERSION', 1);

// base view domain with no leading or trailing slashes
define('EMVIDEO_VZAAR_VIEW_DOMAIN', 'vzaar.com');


/**
 * hook emvideo_PROVIDER_info
 * this returns information relevant to a specific 3rd party video provider
 * @return
 *   an array of strings requested by various admin and other forms
 *   'name' => the translated name of the provider
 *   'url' => the url to the main page for the provider
 *   'settings_description' => a description of the provider that will be posted in the admin settings form
 *   'supported_features' => an array of rows describing the state of certain supported features by the provider.
 *      These will be rendered in a table, with the columns being 'Feature', 'Supported', 'Notes'.
 */
function emvideo_vzaar_info() {
  $features = array(
    array(t('Autoplay'), t('Yes'), ''),
    array(t('RSS Attachment'), t('Yes'), ''),
    array(t('Playlist'), t('Yes'), ''),
  );

  return array(
    'provider' => 'vzaar',
    'name' => t('Vzaar'),
    'url' => EMVIDEO_VZAAR_MAIN_URL,
    'settings_description' => t('These settings specifically affect videos displayed from !provider.', array('!provider' => l(t('vzaar.com'), EMVIDEO_VZAAR_MAIN_URL))),
    'supported_features' => $features,
  );
}

/**
 *  hook emvideo_PROVIDER_settings
 *  This should return a subform to be added to the emvideo_settings() admin
 *  settings page.
 *
 *  Note that a form field set will already be provided at $form['example'],
 *  so if you want specific provider settings within that field set, you should
 *  add the elements to that form array element.
 */
function emvideo_vzaar_settings() {
  $form = array();
  
  $form['vzaar']['player_options'] = array(
    '#type' => 'fieldset',
    '#title' => t('Embedded video player options'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  
  // This is an option to set the video to full screen.
  $form['vzaar']['player_options']['emvideo_vzaar_fullscreen'] = array(
    '#type' => 'checkbox',
    '#title' => t('Allow fullscreen'),
    '#default_value' => variable_get('emvideo_vzaar_fullscreen', 1),
    '#description' => t('Allow users to view video using the entire computer screen.'),
  );
 
  // Looping option
  $form['vzaar']['player_options']['emvideo_vzaar_looping'] = array(
    '#type' => 'checkbox',
    '#title' => t('Looping (autoreplay)'),
    '#default_value' => variable_get('emvideo_vzaar_looping', 0),
  );
  
  // Always show click to play option
  $form['vzaar']['player_options']['emvideo_vzaar_show_play'] = array(
    '#type' => 'checkbox',
    '#title' => t('Always show "click to play" button'),
    '#default_value' => variable_get('emvideo_vzaar_show_play', 0),
  );
  
  // Always show click to play option
  $form['vzaar']['player_options']['emvideo_vzaar_border'] = array(
    '#type' => 'checkbox',
    '#title' => t('Border'),
    '#default_value' => variable_get('emvideo_vzaar_border', 1),
  );
  
  // Colour option dropdown
  $form['vzaar']['player_options']['emvideo_player_colour'] = array(
    '#type' => 'select',
    '#title' => t('Player Colour'),
    '#default_value' => variable_get('emvideo_player_colour', 'black'),
    '#options' => array(
      'black' => t('Default'),
      'blue' => t('Blue'),
      'red' => t('Red'),
      'green' => t('Green'),
      'yellow' => t('Yellow'),
      'pink' => t('Pink'),
      'orange' => t('Orange'),
      'brown' => t('Brown'),
    ),
    '#description' => t('Select the colour of your player.'),
  );
  
  // Brand text fieldset
  $form['vzaar']['player_options']['brand'] = array(
    '#type' => 'fieldset',
    '#title' => t('Branding  settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  
  // Brand text
  $form['vzaar']['player_options']['brand']['emvideo_vzaar_brand_text'] = array(
    '#type' => 'textfield',
    '#title' => t('Brand text'),
    '#default_value' => variable_get('emvideo_vzaar_brand_text', ''),
  );
  
  // Brand text link
  $form['vzaar']['player_options']['brand']['emvideo_vzaar_brand_link'] = array(
    '#type' => 'textfield',
    '#title' => t('Brand text link'),
    '#default_value' => variable_get('emvideo_vzaar_brand_link', ''),
    '#description' => t('please DO NOT start with http://'),
  );
  
  // End text fieldset
  $form['vzaar']['player_options']['end'] = array(
    '#type' => 'fieldset',
    '#title' => t('End text  settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  
  // Brand text
  $form['vzaar']['player_options']['end']['emvideo_vzaar_end_text'] = array(
    '#type' => 'textfield',
    '#title' => t('End text'),
    '#default_value' => variable_get('emvideo_vzaar_end_text', ''),
  );
  
  // Brand text link
  $form['vzaar']['player_options']['end']['emvideo_vzaar_end_link'] = array(
    '#type' => 'textfield',
    '#title' => t('End text link'),
    '#default_value' => variable_get('emvideo_vzaar_end_link', ''),
    '#description' => t('please DO NOT start with http://'),
  );

  return $form;
}


/**
 * hook emvideo_PROVIDER_extract
 * this is called to extract the video code from a pasted URL or embed code.
 * @param $embed
 *   an optional string with the pasted URL or embed code
 * @return
 *   either an array of regex expressions to be tested, or a string with the video code to be used
 *   if the hook tests the code itself, it should return either the string of the video code (if matched), or an empty array.
 *   otherwise, the calling function will handle testing the embed code against each regex string in the returned array.
 */
function emvideo_vzaar_extract($embed = '') {
  $view_domain = preg_quote(EMVIDEO_VZAAR_VIEW_DOMAIN, '@');
  // For playlist
  if (preg_match('@/view.' . $view_domain . '/users/(.*.[flashplaylist])@i', $embed)) {
    return array(
      '@/view.' . $view_domain . '/users/([a-zA-Z0-9]+)\.flashplaylist@i',
    );
  }
  // For single movies
  return array(
    '@/' . $view_domain . '/videos/([0-9]+)@i',
    '@/' . $view_domain . '/users/.*/videos/([0-9]+)@i',
    '@/view.' . $view_domain . '/([a-zA-Z0-9]+)\.flashplayer@i',
  );
}

/**
 *  Implement hook emvideo_PROVIDER_data_version().
 */
function emvideo_vzaar_data_version() {
  return EMVIDEO_VZAAR_DATA_VERSION;
}

/**
 * hook emfield_PROVIDER_data
 *
 * provides an array to be serialised and made available with $item elsewhere
 */
function emvideo_vzaar_data($field, $item) {
  $data = array();
  // adding the version control
  $data['emvideo_vzaar_data_version'] = EMVIDEO_VZAAR_DATA_VERSION;

  // This stores a URL to the video's thumbnail.
  $data['thumbnail'] = 'http://' . EMVIDEO_VZAAR_VIEW_DOMAIN . '/' . $item['value'] . '.thumb';

  // Check if this is playlist
  if (strpos($item['embed'], '.flashplaylist')) {
    $data['playlist'] = TRUE;
  }
  else {
    $data['playlist'] = FALSE;
  }
  
  return $data;
}

/**
 * hook emfield_PROVIDER_rss
 *
 *  This attaches a file to an RSS feed.
 */
function emvideo_vzaar_rss($item, $teaser = NULL) {
  if ($item['value']) {
    $file['thumbnail']['filepath'] = $item['data']['thumbnail'];

    return $file;
  }
}

/**
 * hook emvideo_PROVIDER_embedded_link($video_code)
 * returns a link to view the video at the provider's site.
 *  @param $video_code
 *    The string containing the video to watch.
 *  @return
 *    A string containing the URL to view the video at the original provider's site.
 */
function emvideo_vzaar_embedded_link($video_code) {
  return _emvideo_vzaar_path($video_code);
}

/**
 * _emvideo_vzaar_path().
 * If a single movie, link directly to the movie page.
 * If a playlist, link to user homepage.
 */
function _emvideo_vzaar_path($id) {
  if (preg_match('/^[0-9]{1,}$/', $id)) {
    return EMVIDEO_VZAAR_MAIN_URL . 'videos/' . $id;
  }
  else {
    return EMVIDEO_VZAAR_MAIN_URL . 'users/' . $id;
  }
}

/**
 * hook emvideo_PROVIDER_thumbnail
 * Returns the external url for a thumbnail of a specific video.
 *  @param $field
 *    The field of the requesting node.
 *  @param $item
 *    The actual content of the field from the requesting node.
 *  @return
 *    A URL pointing to the thumbnail.
 *
 *  This does not work with playlist because of multiple images
 */
function emvideo_vzaar_thumbnail($field, $item, $formatter, $node, $width, $height) {
  return $data['thumbnail'];
}

/**
 *  hook emvideo_PROVIDER_video
 *  This actually displays the full/normal-sized video we want, usually on the
 *  default page view.
 *  @param $embed
 *    The video code for the video to embed.
 *  @param $width
 *    The width to display the video.
 *  @param $height
 *    The height to display the video.
 *  @param $field
 *    The field info from the requesting node.
 *  @param $item
 *    The actual content from the field.
 *  @return
 *    The html of the embedded video.
 */
function emvideo_vzaar_video($code, $width, $height, $field, $item, $node,  $autoplay,  $options = array()) {
  $output = theme('emvideo_vzaar_embedded_video', $code, $item, $width, $height, $autoplay);
  return $output;
}

/**
 *  hook emvideo_PROVIDER_preview
 *
 *  This actually displays the preview-sized video we want, commonly for the
 *  teaser.
 *  @param $embed
 *    The video code for the video to embed.
 *  @param $width
 *    The width to display the video.
 *  @param $height
 *    The height to display the video.
 *  @param $field
 *    The field info from the requesting node.
 *  @param $item
 *    The actual content from the field.
 *  @return
 *    The html of the embedded video.
 */
function emvideo_vzaar_preview($code, $width, $height, $field, $item, $node, $autoplay, $options = array()) {
  $output = theme('emvideo_vzaar_embedded_video', $code, $item, $width, $height, $autoplay);
  return $output;
}

/**
 * The embedded flash displaying the video.
 */
function theme_emvideo_vzaar_embedded_video($code, $item, $width, $height, $autoplay) {
  $output = '';
  
  // Assign player settings
  $fullscreen = variable_get('emvideo_vzaar_fullscreen', 1) ? 'true' : 'false';
  $looping = variable_get('emvideo_vzaar_looping', 0);
  $show_play = variable_get('emvideo_vzaar_show_play', 0);
  $border = variable_get('emvideo_vzaar_border', 1);
  $colour = variable_get('emvideo_player_colour', 'default');
  
  // Brand text and link.
  // Make sure we clean them first
  $brand_text = htmlspecialchars(variable_get('emvideo_vzaar_brand_text', ''));
  $brand_link = str_replace('http://', '', variable_get('emvideo_vzaar_brand_link', ''));
  
  // End text and link.
  // Make sure we clean them first
  $end_text = htmlspecialchars(variable_get('emvideo_vzaar_end_text', ''));
  $end_link = str_replace('http://', '', variable_get('emvideo_vzaar_end_link', ''));
  
  // Set flashvars setting
  // Always show play button
  if ($show_play == TRUE) {
    $flashvars = 'showplaybutton=true';
  }
  else {
    $flashvars = 'showplaybutton=false';
  }
  
  // Autoplay setting
  $flashvars .= $autoplay ? '&autoplay=true' : '&autoplay=false';
  
  // Looping setting
  $flashvars .= $looping ? '&looping=true' : '&looping=false';
  
  // Border seting
  if ($border == FALSE) {
    $flashvars .= '&border=none';
  }
  
  // Colour setting
  $flashvars .= '&colourSet='. $colour;
  
  // Brand text setting
  if ($brand_text) {
    $flashvars .= '&brandText='. $brand_text;
    if ($brand_link) {
      $flashvars .= '&brandLink='. $brand_link;
    }
  }
  
  // End text setting
  if ($end_text) {
    $flashvars .= '&endText='. $end_text;
    if ($end_link) {
      $flashvars .= '&endLink='. $end_link;
    }
  }
  
  // For single video
  if ($code && $item['data']['playlist'] == FALSE) {
    $output = '<object class="vzaar_video" width="'. $width .'" height="'. $height .'" type="application/x-shockwave-flash" data="http://view.vzaar.com/'. $code .'.flashplayer">';
    $output .= '<param name="movie" value="http://view.vzaar.com/'. $code .'.flashplayer" />';
    $output .= '<param name="allowScriptAccess" value="always" />';
    $output .= '<param name="allowFullScreen" value="'. $fullscreen .'" />';
    $output .= '<param name="wmode" value="transparent">';
    $output .= '<param name="flashvars" value="'. $flashvars .'" />';
    $output .= '<param name="quality" value="best" />';
    $output .= '<embed src="http://view.vzaar.com/'. $code .'.flashplayer" type="application/x-shockwave-flash" wmode="transparent" width="'. $width .'" height="'. $height .'" allowScriptAccess="always" allowFullScreen="'. $fullscreen .'" flashvars="'. $flashvars .'"></embed>';
    $output .= '<video width="'. $width .'" height="'. $height .'" src="http://view.vzaar.com/'. $code .'.mobile" poster="http://view.vzaar.com/'. $code .'.image" controls></video>';
    $output .= '</object>';
  }
  elseif ($code) { // For playlist player
    $output = '<object class="vzaar_video" width="'. $width .'" height="'. $height .'" type="application/x-shockwave-flash" data="http://view.vzaar.com/users/'. $code .'.flashplaylist">';
    $output .= '<param name="movie" value="http://view.vzaar.com/users/'. $code .'.flashplaylist" />';
    $output .= '<param name="allowScriptAccess" value="always" />';
    $output .= '<param name="allowFullScreen" value="'. $fullscreen .'" />';
    $output .= '<param name="wmode" value="transparent">';
    $output .= '<param name="flashvars" value="'. $flashvars .'" />';
    $output .= '<param name="quality" value="best" />';
    $output .= '<embed src="http://view.vzaar.com/users/'. $code .'.flashplaylist" type="application/x-shockwave-flash" wmode="transparent" width="'. $width .'" height="'. $height .'" allowScriptAccess="always" allowFullScreen="'. $fullscreen .'" flashvars="'. $flashvars .'"></embed>';
    $output .= '</object>';
  }
  return $output;
}

/**
 * Implementation of hook_emfield_subtheme.
 */
function emvideo_vzaar_emfield_subtheme() {
  return array(
    'emvideo_vzaar_embedded_video' => array(
      'arguments' => array('item' => NULL, 'width' => NULL, 'height' => NULL, 'autoplay' => NULL),
      'file' => 'providers/vzaar.inc'
    )
  );
}
